import React from 'react';  
import './App.css';
import styled from 'styled-components';

const Button = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center; 
    font-size: 14px;  
    background: #26a69a;
    border: 1px solid #01695c;
    border-radius: 25px;
    transition: transform 0.2s;
    :hover{
        transform: scale(0.98)
    }  
    padding: 5px;
`;
const ButtonText = styled.p`
    font-size: 12px;
    color: #0a0a0a;
    text-align: center; 
    text-vertical-align: center;
    padding-bottom: 3px;
    cursor: pointer;
`;


class File extends React.Component<any,any> {
    constructor(props: any) {
      super(props)
      
      const defaultFileType = "json"; 
      this.fileNames = {
          json: `${this.props.format === 'portal' ? 'appsettings' : 'mobilesettings'}.json`,
        csv: "states.csv",
        text: "states.txt"
      }    
      this.state = {
        fileType: defaultFileType,
        fileDownloadUrl: null,
        json: this.props.json,
        status: "",
          data: [
            { state: "Arizona",        electors: 11 },
            { state: "Florida",        electors: 29 },
            { state: "Iowa",           electors:  6 },
            { state: "Michigan",       electors: 16 },
            { state: "North Carolina", electors: 15 },
            { state: "Ohio",           electors: 18 },
            { state: "Pennsylvania",   electors: 20 },
            { state: "Wisconsin",      electors: 10 },
        ]
      }
        this.changeFileType = this.changeFileType.bind(this);
        this.download = this.download.bind(this);
        this.upload = this.upload.bind(this);
        this.openFile = this.openFile.bind(this);
    }
    fileNames: any = null;
    dofileUpload: any = null;
    dofileDownload: any = null;
    changeFileType (event: any) {
      const value = event.target.value;
        this.setState({fileType: value});
    }
    
    download (event: any) {
      event.preventDefault();
        // Prepare the file
      let output: any;
      if (this.state.fileType === "json") {
          let json:any = this.state.json;
          let res:any = {};
          Object.keys(json).map((field, index)=>{
            console.debug(Object.values(json)[index])
            let data:any = Object.values(json)[index];
            if(!data.title)
              return null
              res[field] = data.val; 
          }) 
        //   output = JSON.stringify({states: this.state.data}, 
             output = JSON.stringify(res,
            null, 4);
      } else if (this.state.fileType === "csv"){
        // Prepare data:
        let contents = [];
        contents.push (["State", "Electors"]);
        this.state.data.forEach((row: any) => {
            contents.push([row.state, row.electors])
        });
        output = this.makeCSV(contents);
      } else if (this.state.fileType === "text"){
        // Prepare data:
        output = '';
        this.state.data.forEach((row: any) => {
            output += `${row.state}: ${row.electors}\n`
        });
      }
      // Download it
      const blob = new Blob([output]);
      const fileDownloadUrl = URL.createObjectURL(blob);
      this.setState ({fileDownloadUrl: fileDownloadUrl}, 
        () => {
          this.dofileDownload.click(); 
          URL.revokeObjectURL(fileDownloadUrl);  // free up storage--no longer needed.
          this.setState({fileDownloadUrl: ""})
      })    
    }
  
    /**
     * Function returns the content as a CSV string
     * See https://stackoverflow.com/a/20623188/64904
     * Parameter content:
     *   [
     *.     [header1, header2],
     *.     [data1, data2]
     *.     ...
     *.  ]
     * NB Does not support Date items
     */
    makeCSV (content: any) {
        let csv = '';
      content.forEach((value:any) => {
          value.forEach((item: any, i: any) => {
          let innerValue = item === null ? '' : item.toString();
          let result = innerValue.replace(/"/g, '""');
          if (result.search(/("|,|\n)/g) >= 0) {
            result = '"' + result + '"'
          }
          if (i > 0) {csv += ','}
          csv += result;
        })
          csv += '\n';
        })
      return csv
    }
    
    upload(event: any) {
        event.preventDefault();
      this.dofileUpload.click()
    }
    
    /**
     * Process the file within the React app. We're NOT uploading it to the server!
     */
    openFile(evt: any) {
        let status: any = []; // Status output
        const fileObj = evt.target.files[0];
        const reader = new FileReader();
            
        let fileloaded = (e: any) => {
          // e.target.result is the file's content as text
          const fileContents = e.target.result;
          status.push(`File name: "${fileObj.name}". Length: ${fileContents.length} bytes.`);
          // Show first 80 characters of the file
          const first80char = fileContents.substring(0,80);
          status.push (`First 80 characters of the file:\n${first80char}`)
          this.setState ({status: status.join("\n")})
        }
        
        // Mainline of the method
        fileloaded = fileloaded.bind(this);
        reader.onload = fileloaded;
        reader.readAsText(fileObj);  
    }
    
    render() {
      return (
        <div className={'file-bar'}> 
          <form>
            {/* <select name="fileType"
              onChange={this.changeFileType}
              value={this.state.fileType}
              className="mr"
            >
              <option value="csv">CSV</option>
              <option value="json">JSON</option>
              <option value="text">Text</option>
            </select>  */}
             
             <a
                download={ this.state.fileType === 'json' ?`${this.props.format === 'portal' ? 'appsettings' : 'mobilesettings'}.json` : this.fileNames[this.state.fileType]}
                href={this.state.fileDownloadUrl}
                ref={e=>this.dofileDownload = e}
             > </a>
            
            <Button onClick={this.props.format === '' ? ()=> alert('בחר סוג קובץ!') : this.download}>
              <ButtonText>
                  הורדת קובץ
              </ButtonText>
            </Button>
            
            {/* <p><button onClick={this.upload}>
              Upload a file!
            </button> Only json, csv, and text files are ok.</p>
  
            <input type="file" className="hidden"
              multiple={false}
              accept=".json,.csv,.txt,.text,application/json,text/csv,text/plain"
              onChange={evt => this.openFile(evt)}
              ref={e=>this.dofileUpload = e}
            /> */}
          </form>
          <pre className="status">{this.state.status}</pre>
        </div>
        )
    }
  }
  
  export default File;
  