import React, { useState } from 'react'; 
import './App.css';
import File from './File';
import styled from 'styled-components';
import Switch from "react-switch";
import { FaMobileAlt, FaDesktop } from 'react-icons/fa';
const language = {
  headerTitle: 'Softsolutions',
  headerSubTitle: 'JSON הגדרת קובץ',
  headerBarBtnPortal: 'פורטל',
  headerBarBtnApp: 'אפליקציה',
}

const json = {
  "username": {val:"demo", title: 'שם משתמש פריוריטי', type: 'text', pos: 'same'}, //
  "password": {val:"12345",title: 'סיסמה פריוריטי', type: 'text', pos: 'same'}, //
  "url":  {val:"https://demo.softsolutions.co.il",title: 'כתובת שרת', type: 'text', pos: 'same'}, //
  "tabulaini":  {val:"tabula.ini",title: 'שם קובץ טבולה', type: 'text', pos: 'same'}, //
  "company":  {val:"demoout",title: 'חברה', type: 'text', pos: 'same'}, //
  "profileConfig": { val:{"company": "demoout"} },//
  "appname":  {val:"portal",title: 'שם אפליקציה', type: 'text', pos: 'same'}, //
  "devicename":  {val:"",title: 'שם מכשיר', type: 'text', pos: 'same'}, //
  "debug":  {val:false,title: 'בדיקות', type: 'boolean', pos: 'same'}, //
  "isIgnoreCase":  {val:false,title: 'חפש לפי אותיות קטנות', type: 'boolean', pos: 'same'}, //
  "defaultUserType":  {val:"none",title: 'טיפוס ברירת מחדל', type: 'text', pos: 'same'}, //
  "showFreeTextSearch" :   {val:true,title: 'הצג טקסט חופשי', type: 'boolean', pos: 'same'}, //
  "primaryColor":  {val:"#174768",title: 'צבע ראשי', type: 'text', pos: 'same'}, //
  "secondaryColor": {val: "#48C0EE",title: 'שם משני', type: 'text', pos: 'same'}, //
  "useBPMFilter":  {val:true,title: 'BPMFהשתמש ב', type: 'boolean', pos: 'same'}, //
//app
  "language":  {val:1,title: 'שפה', type: 'number', pos: 'app'}, //
  "logoLoginURL":  {val:"https://demo.softsolutions.co.il/Images/softlogo.png", title: 'תמונה ראשית', type: 'text', pos: 'app'},
  "usePDFReader":  {val:true, title: 'הצג נספחים', type: 'boolean' , pos: 'app'}, 
  "showSubFormsFab":  {val:true, title: 'הצג מסכי בן', type: 'boolean', pos: 'app'}, 
  "hiddenSaveBarOnAutoSave":  {val:false, title: 'הסתר כפתורי שמור/ביטול במצב אוטומטי', type: 'boolean', pos: 'app'}, 
  "displayCompanyInAllForms":  {val:false, title: 'הצג בר צבעוני שם חברה', type: 'boolean', pos: 'app'}, 
//portal
  "lang" : {val:"HEB", title: 'שפה', type: 'text', pos: 'portal'},
  "dir" :  {val:"rtl"},
  "apkLink":  {val:"https://expo.io/artifacts/2073d44f-7fbb-4e1c-b416-20108b90c1ca", title: 'APK לינק', type: 'text', pos: 'portal'},
  "waitImgName":  {val:"", title: 'תמונה מסך כניסה', type: 'text', pos: 'portal'},
  "showQuestionnaireBtn":  {val:true, title: 'הצג כפתור שאלונים', type: 'boolean', pos: 'portal'}, 
  "logoAppBarName":  {val:"softlogo.png", title: 'שם לוגו בר', type: 'text', pos: 'portal'},
  "logoLoginName":  {val:"bg-demo.jpg", title: 'שם מסך כניסה', type: 'text', pos: 'portal'},
  "iosLink":  {val:"https://apps.apple.com/us/app/prisoft/id1488322061?ign-mpt=uo%3D2", title: 'IOS לינק', type: 'text', pos: 'portal'},
  "idleTimeInMinutes":  {val:10, title: 'זמן טעינה בדקות', type: 'number', pos: 'portal'},
  "QuantFieldsInRow":  {val:4, title: 'כמות שדות בשורה', type: 'number', pos: 'portal'},
  "LoginCompanyName":  {val:"שם חברה מסך כניסה",title: 'שם משתמש פריוריטי', type: 'text', pos: 'portal'},
  "browserTitle":  {val:"PriPortal Demo",title: 'כותרת לדפדפן', type: 'text', pos: 'portal'},
  "dashboardRefreshTimeInSeconds":  {val:600, title: 'רענון מסך בית בשניות', type: 'number', pos: 'portal'},
  "useAPIForValidation":  {val:1, title: 'APIהשתמש ב', type: 'number', pos: 'portal'},
  "directLinkFinishText":  {val:"הפעולה בוצעה בהצלחה - תודה ושלום - תוכן מקובץ הגדרות",title: 'טקסט להצגה בסוף הפעולה', type: 'text', pos: 'portal'},
  "directLinkFinishBackground":  {val:"bg-demo.jpg",title: 'תמונה להצגה בסוף פעולה', type: 'text', pos: 'portal'},
  "dashboardImgName":  {val:"bg-demo.jpg",title: 'תמונה מסך בית', type: 'text', pos: 'portal'},
  "rememberLastCompany":  {val:true, title: 'שמירת החברה האחרונה', type: 'boolean', pos: 'portal'}, 
  "closeSideBar":  {val:false, title: 'סגור תפריט', type: 'boolean', pos: 'portal'}, 
  "closeSideBarOnClick":  {val:true, title: 'סגור תפריט בלחיצה', type: 'boolean', pos: 'portal'}, 
  "useButtonForSearch" :   {val:true, title: 'השתמש בכפתור לרענון', type: 'boolean', pos: 'portal'}, 
  "autoLogin": {val: true, title: 'התחברות אוטומטית', type: 'boolean', pos: 'portal'}, 
  "flipTableScrollBar":  {val:false, title: 'הפוך סרגל גלילה', type: 'boolean', pos: 'portal'}, 
  "useTextColorOnPaintRow" :  {val:false, title: 'השתמש בטקסט לצביעת שורה', type: 'boolean', pos: 'portal'}, 
  "usePlusMinusInTable":  {val:true, title: 'השתמש בכפתור פלוס/מינוס', type: 'boolean', pos: 'portal'}, 
 };

 const Header = styled.div`
  min-height: 130px;
  min-width: 100vw;
  background-color: #00685c; 
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  font-size: calc(10px + 2vmin);
  color: #2eaa9f; 
  padding: 10px;
  -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
  position: fixed;
  top: 0;
  z-index:2;
 `;

 const HeaderBar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center; 
  font-size: 14px;  
 `;

 const HeaderBarBtn = styled.p<any>`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center; 
  font-size: 14px; 
  cursor: pointer; 
  transition: transform 0.2s;
  :hover{
    color: #164741;
    transform: scale(0.98)
  } 
  color: ${(props)=>props.format === props.type ? '#fff' : '#2eaa9f'}; 
  text-align: center;
  min-width: 100px;
 `;

 const HeaderTitle = styled.h2<any>` 
  font-size: ${(props)=>props.fontSize};
  text-align: center;  
  color: ${(props)=>props.color};
`;

const InputLabel = styled.label<any>` 
 font-size: 18px;
 text-align: center;  
 color: ${(props)=>props.color};
 margin-right: 15px; 
`;

const InputValue = styled.input<any>` 
 font-size: 16px;
 text-align: center; 
 max-width: 150px;
 border: 1px solid #cecece;
`;

const InputApp = styled.div`
 display: flex;
 flex-direction: row;
 align-items: center;
 justify-content: space-between; 
 width: 100%; 
 margin: 5px;
 :hover{
   background: #ececec;
 }
`;
const MainView = styled.div` 
 display: flex;
 flex-direction: column;
 align-items: flex-end;
 justify-content: flex-start;
 width: 90vw; 
 max-width: 1200px;
 min-width: 1000px; 
 padding: 20px; 
 overflow-y: visible;
 padding-top: 140px;  
`; 
const BackImg = styled.div` 
  width: 250px;
  height: 250px;
  position: fixed;
  opacity: 0.3;
  top: 350px;
  z-index: -1;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('https://static.wixstatic.com/media/a7f08a_53033405b5714567a19d184cf3d2cf18~mv2.png/v1/fill/w_89,h_89,al_c,q_85,usm_0.66_1.00_0.01/Original%20on%20Transparent%20-%20300.webp');
`
function Input(props: any) {
  const [checked,setChecked] = useState(props.val); 
  return(
    <InputApp onClick={()=>setChecked(!checked)}>
      {props.type === 'boolean' ? 
         <Switch
            checked={checked}
            onChange={setChecked}
            onColor="#9ed7d2"
            offColor="#cecece"
            onHandleColor="#26a69a"
            handleDiameter={30}
            uncheckedIcon={false}
            checkedIcon={false}
            boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
            activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
            height={20}
            width={48}
            className="react-switch"
            id="material-switch"
          /> : <InputValue value={props.val}/> }
          <InputLabel>
            {props.label}
          </InputLabel>
    </InputApp>
  )
}

function App() {
  const [format,setFormat] = useState('');
  function getJsonFields(){  
    return(
      Object.keys(json).map((field, index)=>{
        console.debug(Object.values(json)[index])
        let data:any = Object.values(json)[index];
        if(!data.title)
          return null
        return <Input val={data.val} type={data.type} label={`${data.title}`}/>
      })
    )
  }
  return (  
  <div className={'App'}>
    <BackImg/>
    <Header>
      <div style={{position:'absolute',top:10}}>
        <HeaderTitle color={'#fff'} fontSize={'1.5em'}>
          {language.headerTitle}
        </HeaderTitle>  
        <HeaderTitle color={'#0a0a0a'} fontSize={'0.5em'}>
          {language.headerSubTitle}
        </HeaderTitle>   
      </div>
      <HeaderBar> 
        <HeaderBarBtn type={'portal'} onClick={()=>setFormat('portal')} format={format}>
          {language.headerBarBtnPortal} 
          <FaDesktop style={{marginLeft:5}}/>
        </HeaderBarBtn>
        <HeaderBarBtn type={'app'} onClick={()=>setFormat('app')} format={format}>
          {language.headerBarBtnApp} 
          <FaMobileAlt style={{marginLeft:5}}/> 
        </HeaderBarBtn>
      </HeaderBar>
    </Header> 
    <MainView>
      <File format={format} json={json}/>
      {getJsonFields()}
    </MainView> 
  </div>
  );
}

export default App;
